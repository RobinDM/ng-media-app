# MediaApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

## Running the code from source

1. Run `npm install` from the root of the project.
2. Run `ng serve` to launch a web server that hosts the project. It should listen on `http://localhost:4200/`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

