import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ActorsService } from '../actors.service';
import {Actor} from 'src/app/actor';

@Component({
    selector: 'media-actor-detail',
    templateUrl: './actor-detail.component.html',
    styleUrls: ['./actor-detail.component.css']
})
export class ActorDetailComponent implements OnInit {

    public actor: Actor;

    constructor(
        private activeRoute: ActivatedRoute,
        private actorsService: ActorsService
    ) { }

    ngOnInit() {
        // this.activeRoute.paramMap.subscribe((params:ParamMap) => {
        //     const id = +params.get('id');
        //     this.actor = this.actorsService.getActor(id);
        // });
        const self = this;
        this.activeRoute.paramMap.subscribe(function(paramMap: ParamMap) {
            const id = +paramMap.get('id');
            self.actor = self.actorsService.getActor(id);
        });
    }

}
