import { Component, OnInit } from '@angular/core';
import { ActorsService } from '../actors.service';
import {Actor} from 'src/app/actor';

@Component({
    selector: 'media-actor-list',
    templateUrl: './actor-list.component.html',
    styleUrls: ['./actor-list.component.css']
})
export class ActorListComponent implements OnInit {

    public actors: Actor[];

    constructor(private actorsService: ActorsService) { }

    ngOnInit() {
        this.actors = this.actorsService.getActors();
    }

}
