import { Component, OnInit } from '@angular/core';
import {ActorsService} from 'src/app/actors.service';

@Component({
  selector: 'media-actor-overview',
  templateUrl: './actor-overview.component.html',
  styleUrls: ['./actor-overview.component.css']
})
export class ActorOverviewComponent implements OnInit {

  constructor(private actorsService: ActorsService) { }

  ngOnInit() {
  }

}
