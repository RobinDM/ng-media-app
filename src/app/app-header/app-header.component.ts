import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../movies/movies.service';
import { Movie } from '../movies/movie';

@Component({
    selector: 'media-app-header',
    templateUrl: './app-header.component.html',
    styleUrls: ['./app-header.component.css']
})
export class AppHeaderComponent implements OnInit {
    constructor(private moviesService: MoviesService) { }

    public ngOnInit() {
    }

    public addShrek() {
        this.moviesService.lookupMovie('shrek').subscribe(
            (shreks: Movie[]) => shreks.forEach(s => {
                this.moviesService.addMovie(s.onlineId, false).subscribe((m: Movie) => {
                    console.log('added', m.title);
                });
            })
        );
    }
}
