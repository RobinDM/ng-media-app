import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './media.routes';

import { AppComponent } from './app.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { ActorOverviewComponent } from './actor-overview/actor-overview.component';
import { ActorListComponent } from './actor-list/actor-list.component';
import { ActorDetailComponent } from './actor-detail/actor-detail.component';
import { ActorAddComponent } from './actor-add/actor-add.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {TestInterceptor} from 'src/app/test-interceptor';
import {MoviesModule} from 'src/app/movies/movies.module';
import {TvShowsModule} from 'src/app/tv-shows/tv-shows.module';

@NgModule({
    declarations: [
        AppComponent,
        AppHeaderComponent,
        ActorOverviewComponent,
        ActorListComponent,
        ActorDetailComponent,
        ActorAddComponent,
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes),
        HttpClientModule,
        MoviesModule,
        TvShowsModule
    ],
    providers: [{
        provide: HTTP_INTERCEPTORS,
        useClass: TestInterceptor,
        multi: true
    }],
    bootstrap: [AppComponent]
})
export class AppModule { }
