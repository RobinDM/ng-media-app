import { Routes } from '@angular/router';
import {ActorOverviewComponent} from 'src/app/actor-overview/actor-overview.component';
import {ActorDetailComponent} from 'src/app/actor-detail/actor-detail.component';
import {ActorAddComponent} from 'src/app/actor-add/actor-add.component';
import { MoviesListComponent } from './movies/movies-list/movies-list.component';
import { MoviesAddComponent } from './movies/movies-add/movies-add.component';
import { MoviesDetailComponent } from './movies/movies-detail/movies-detail.component';
import {TvShowWatchListComponent} from 'src/app/tv-shows/tv-show-watch-list.component';
import {TvShowDetailsComponent} from 'src/app/tv-shows/tv-show-details/tv-show-details.component';
import { TvShowAddComponent } from './tv-shows/tv-show-add/tv-show-add.component';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'movies',
        pathMatch: 'full',
    },
    {
        path: 'movies',
        component: MoviesListComponent
    },
    {
        path: 'movies/add',
        component: MoviesAddComponent
    },
    {
        path: 'movies/:id',
        component: MoviesDetailComponent
    },
    {
        path: 'actors',
        component: ActorOverviewComponent,
        children: [
            {
                path: 'add',
                component: ActorAddComponent
            },
            {
                path: ':id',
                component: ActorDetailComponent
            }
        ]
    },
    {
        path: 'tvshows',
        component: TvShowWatchListComponent
    },
    {
        path: 'tvshows/search',
        component: TvShowAddComponent
    },
    {
        path: 'tvshows/:id',
        component: TvShowDetailsComponent
    },
];
