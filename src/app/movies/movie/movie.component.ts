import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Movie } from '../movie';

@Component({
    selector: 'media-movie',
    templateUrl: './movie.component.html',
    styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {

    @Input() public movie: Movie;
    @Output() public selected = new EventEmitter<Movie>();

    constructor() { }

    ngOnInit() {
    }

}
