import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { MoviesService } from '../movies.service';
import { Movie } from '../movie';

@Component({
    selector: 'media-movies-add',
    templateUrl: './movies-add.component.html',
    styleUrls: ['./movies-add.component.css']
})
export class MoviesAddComponent implements OnInit {

    public movies: Observable<Movie[]>;

    constructor(
        private moviesService: MoviesService,
        private router: Router,
    ) { }

    ngOnInit() {
    }

    public lookupRx(term: string): void {
        this.movies = this.moviesService.searchOnline(term);
    }

    public add(movie: Movie): void {
        // so this clearly doesn't work...
        this.moviesService.addMovie(movie.onlineId, false).subscribe(
            (m: Movie) => {
                this.router.navigate(['/movies', m.id]);
            },
            error => console.log('Add failed!', error)
        );
    }
}
