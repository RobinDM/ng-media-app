import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../movies.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Movie } from '../movie';

@Component({
    selector: 'media-movies-detail',
    templateUrl: './movies-detail.component.html',
    styleUrls: ['./movies-detail.component.css']
})
export class MoviesDetailComponent implements OnInit {
    public movie: Movie;

    constructor(
        private moviesService: MoviesService,
        private activeRoute: ActivatedRoute,
        private router: Router
    ) { }

    public ngOnInit() {
        const movieId = +this.activeRoute.snapshot.paramMap.get('id');
        this.moviesService.getMovie(movieId).subscribe((movie: Movie) => this.movie = movie);
    }

    public goBack(): void {
        this.router.navigate(["/movies"]);
    }

    public delete(): void {
        this.moviesService.deleteMovie(this.movie.id).subscribe( () => this.router.navigate(['/movies']));
    }
}
