import { Component, OnInit } from '@angular/core';
import { Movie } from './../movie';
import { MoviesService } from './../movies.service';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';

@Component({
    selector: 'media-movies-list',
    templateUrl: './movies-list.component.html',
    styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit {

    public movies: Observable<Movie[]>;

    constructor(
        private moviesService: MoviesService,
        private router: Router
    ) { }

    public lookupRx({term, privateCollection}): void {
        if (term.length < 2) {
            this.movies = this.moviesService.getMovies();
        } else {
            this.movies = this.moviesService.searchInCollection(term, privateCollection);
        }
    }

    public ngOnInit() {
        this.movies = this.moviesService.getMovies();
    }

    public navigateTo(movie: Movie): void {
        this.router.navigate(['/movies', movie.id]);
    }
}
