import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieComponent } from './movie/movie.component';
import { MoviesAddComponent } from './movies-add/movies-add.component';
import { MoviesDetailComponent } from './movies-detail/movies-detail.component';
import { MoviesListComponent } from './movies-list/movies-list.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    declarations: [
        MoviesListComponent,
        MoviesDetailComponent,
        MoviesAddComponent,
        MovieComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
    ],
    exports: [
        MovieComponent,
        MoviesAddComponent,
        MoviesDetailComponent,
        MoviesListComponent,
    ],
})
export class MoviesModule { }
