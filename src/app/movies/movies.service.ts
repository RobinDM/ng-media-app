import { Injectable } from '@angular/core';
import { Movie } from './movie';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from './../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class MoviesService {

    constructor(private http: HttpClient) { }

    public getMovies(): Observable<Movie[]> {
        return this.http.get<Movie[]>(environment.apiUrl);
    }

    public getMovie(id: number): Observable<Movie> {
        return this.http.get<Movie>(`${environment.apiUrl}/${id}`);
    }

    public addMovie(onlineId: string, isPrivate: boolean): Observable<Movie> {
        return this.http.post<Movie>(`${environment.apiUrl}?public=${!isPrivate}`, {
            apiId: onlineId
        });
    }

    public lookupMovie(title: string): Observable<Movie[]> {
        return this.searchMovies(title, true, false);
    }

    public searchOnline(title: string): Observable<Movie[]> {
        return this.searchMovies(title, true, false);
    }

    public searchInCollection(title: string, privateCollection: boolean): Observable<Movie[]> {
        return this.searchMovies(title, false, privateCollection);
    }

    private searchMovies(title:string, online: boolean, privateCollection: boolean): Observable<Movie[]> {
        return this.http.get<Movie[]>(
            `${environment.apiUrl}/search`, {
            params: new HttpParams()
                .set('title', title)
                .set('online', `${online}`)
                .set('public', `${!privateCollection}`)
        });
    }

    public deleteMovie(id: number): Observable<void> {
        return this.http.delete<void>(`${environment.apiUrl}/${id}`);
    }
}
