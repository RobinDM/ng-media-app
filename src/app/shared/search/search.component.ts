import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Component({
    selector: 'media-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, OnDestroy {

    @Output() search = new EventEmitter<any>();

    public search$ = new Subject<string>();

    constructor() { }

    ngOnInit() {
        this.search$.pipe(
            debounceTime(400),
            distinctUntilChanged(),
            // filter(t => t.length > 1)
        ).subscribe(
            (t: string) => this.search.emit({
                term: t,
                privateCollection: true
            }),
            (err: any) => console.log('error:', err),
            () => console.log('finished!')
        );
    }

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnDestroy(): void {
        this.search$.unsubscribe();
    }
}
