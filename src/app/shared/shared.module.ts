import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MediaComponent } from './media/media.component';
import { SearchComponent } from './search/search.component';
import {FormsModule} from '@angular/forms';



@NgModule({
    declarations: [
        SearchComponent,
        MediaComponent],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        SearchComponent,
        MediaComponent
    ]
})
export class SharedModule { }
