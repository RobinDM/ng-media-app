import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class TestInterceptor implements HttpInterceptor {
    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log('in my own interceptor!');
        const cloned = req.clone({
            headers: req.headers.set('Authorization', 'AlteracValleyIsMyHomeNow')
        });
        return next.handle(cloned);
    }
}
