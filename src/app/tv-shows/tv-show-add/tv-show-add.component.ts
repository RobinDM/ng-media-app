import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {TvShow} from 'src/app/tv-shows/tv-show';
import {TvShowsService} from 'src/app/tv-shows/tv-shows.service';

@Component({
    selector: 'media-tv-show-add',
    templateUrl: './tv-show-add.component.html',
    styleUrls: ['./tv-show-add.component.css']
})
export class TvShowAddComponent implements OnInit {
    public shows: Observable<TvShow[]>;

    constructor(private service: TvShowsService) { }

    ngOnInit() {
    }

    public search(term: string): void {
        console.log('should search for:', term);
        this.shows = this.service.search(term);
    }

    public add(show: TvShow): void {
        console.log('adding show', show);
        this.service.addToCollection(`${show.onlineId}`).subscribe(
            (s: TvShow) => console.log('added show:', s),
            err => console.log('error!', err)
        );
    }
}
