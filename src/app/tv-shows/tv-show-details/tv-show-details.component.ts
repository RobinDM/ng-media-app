import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { TvShowsService } from '../tv-shows.service';
import { TvShow } from '../tv-show';

@Component({
    selector: 'media-tv-show-details',
    templateUrl: './tv-show-details.component.html',
    styleUrls: ['./tv-show-details.component.css']
})
export class TvShowDetailsComponent implements OnInit {

    public show: TvShow;

    constructor(
        private activeRoute: ActivatedRoute,
        private service: TvShowsService
    ) { }

    ngOnInit() {
        this.activeRoute.paramMap.subscribe(
            (paramMap: ParamMap) => {
                this.service.getShowDetails(+paramMap.get('id')).subscribe(
                    (show: TvShow) => this.show = show
                );
            }
        );
    }

}
