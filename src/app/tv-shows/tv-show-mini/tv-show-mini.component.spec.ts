import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TvShowMiniComponent } from './tv-show-mini.component';

describe('TvShowMiniComponent', () => {
  let component: TvShowMiniComponent;
  let fixture: ComponentFixture<TvShowMiniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TvShowMiniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TvShowMiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
