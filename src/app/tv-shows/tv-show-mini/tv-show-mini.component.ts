import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {TvShow} from 'src/app/tv-shows/tv-show';

@Component({
    selector: 'media-tv-show-mini',
    templateUrl: './tv-show-mini.component.html',
    styleUrls: ['./tv-show-mini.component.css']
})
export class TvShowMiniComponent implements OnInit {

    @Input() show: TvShow;
    @Output() clicked = new EventEmitter<TvShow>();

    constructor() { }

    ngOnInit() {
    }

}
