import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TvShowWatchListComponent } from './tv-show-watch-list.component';

describe('TvShowWatchListComponent', () => {
  let component: TvShowWatchListComponent;
  let fixture: ComponentFixture<TvShowWatchListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TvShowWatchListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TvShowWatchListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
