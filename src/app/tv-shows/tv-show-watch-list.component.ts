import { Component, OnInit } from '@angular/core';
import {TvShowsService} from 'src/app/tv-shows/tv-shows.service';
import {Observable} from 'rxjs';
import {TvShow} from 'src/app/tv-shows/tv-show';
import {Router} from '@angular/router';

@Component({
    selector: 'media-tv-show-watch-list',
    templateUrl: './tv-show-watch-list.component.html',
    styleUrls: ['./tv-show-watch-list.component.css']
})
export class TvShowWatchListComponent implements OnInit {
    public shows: Observable<TvShow[]>;

    constructor(private service: TvShowsService, private router: Router) { }

    ngOnInit() {
        this.shows = this.service.getWatchList();
    }

    public goToDetail(show: TvShow): void {
        this.router.navigate(['/tvshows', show.id]);
    }
}
