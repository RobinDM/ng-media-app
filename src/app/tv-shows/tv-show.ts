export interface TvShow {
    id: number;
    onlineId: string;
    overview: string;
    poster: string;
    rating: number;
    runtime: number;
    title: string;
    year: number;
    status: string;
}
