import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TvShowDetailsComponent } from './tv-show-details/tv-show-details.component';
import { TvShowWatchListComponent } from './tv-show-watch-list.component';
import { HttpClientModule } from '@angular/common/http';
import { TvShowMiniComponent } from './tv-show-mini/tv-show-mini.component';
import { TvShowAddComponent } from './tv-show-add/tv-show-add.component';
import { SharedModule } from '../shared/shared.module';



@NgModule({
    declarations: [
        TvShowDetailsComponent,
        TvShowWatchListComponent,
        TvShowMiniComponent,
        TvShowAddComponent,
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        SharedModule
    ],
    exports: [
    ],
})
export class TvShowsModule { }
