import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import {Observable} from 'rxjs';
import {TvShow} from 'src/app/tv-shows/tv-show';
import {HttpClient, HttpParams} from '@angular/common/http';


@Injectable({
    providedIn: 'root'
})
export class TvShowsService {

    constructor(private http: HttpClient) { }

    public getWatchList(): Observable<TvShow[]> {
        return this.http.get<TvShow[]>(environment.tvShowsUrl);
    }

    public search(term: string, online: boolean = true): Observable<TvShow[]> {
        return this.http.get<TvShow[]>(`${environment.tvShowsUrl}/search`, {
            params: new HttpParams().set('title', term).set('online', `${online}`)
        });
    }

    public addToCollection(onlineId: string): Observable<TvShow> {
        console.log('adding to collection:', onlineId);
        return this.http.post<TvShow>(`${environment.tvShowsUrl}/watchlist`, {
            apiId: onlineId
        });
    }

    public getShowDetails(id: number): Observable<TvShow> {
        return this.http.get<TvShow>(`${environment.tvShowsUrl}/${id}`);
    }
}
